import $ from 'jquery';
import 'bootstrap';

$(document).ready(function () {
    console.log(23);

    const slid = $('.nav-item_slid');
    slid.on('click', function () {
        $('.submenu').slideToggle('fast');
    });

    $(document).mouseup(function (e) {
        if (!slid.is(e.target) && slid.has(e.target).length === 0) {
            $(".submenu").slideUp();
        }
    });

    const hamburger = $('.hamburger');
    const connect = $('.connect');
    const navbarCollapse = $('.navbar-collapse');

    hamburger.on('click', function () {
        hamburger.toggleClass('is-active');
        connect.toggleClass('dark');
    });

    $(document).mouseup(function (e) {
        if (!hamburger.is(e.target) &&
            hamburger.has(e.target).length === 0 &&
            !navbarCollapse.is(e.target) &&
            navbarCollapse.has(e.target).length === 0) {

                $('.navbar-collapse').removeClass('show');
                hamburger.removeClass('is-active');
                connect.removeClass('dark');
        }
    });

    $('.span-circle-item').on('click', function() {
        let link = $(this).attr('data-link');
        $('.link-buy').attr('href', link);

        $('.span-circle').removeClass('active');
        $(this).find('.span-circle').addClass('active');
    })
});